%global pkg aacraid

%global kernel 5.14.0-362.18.1.el9_3
%global baserelease 2

%global debug_package %{nil}

%global __spec_install_post \
  %{?__debug_package:%{__debug_install_post}} \
  %{__arch_install_post} \
  %{__os_install_post} \
  %{__mod_compress_install_post}

%global __mod_compress_install_post %{nil}


Name:             kmod-%{pkg}
Epoch:            1
Version:          %(echo %{kernel} | sed 's/-/~/g; s/\.el.*$//g')
Release:          %{baserelease}%{?dist}
Summary:          Dell PERC2, 2/Si, 3/Si, 3/Di, Adaptec Advanced Raid Products, HP NetRAID-4M, IBM ServeRAID & ICP SCSI (%{pkg}) driver

License:          GPLv2 and GPLv2+
URL:              https://www.kernel.org/

Patch0:           source-git.patch

ExclusiveArch:    x86_64 ppc64le

BuildRequires:    elfutils-libelf-devel
BuildRequires:    gcc
BuildRequires:    kernel-rpm-macros
BuildRequires:    kmod
BuildRequires:    make
BuildRequires:    redhat-rpm-config

BuildRequires:    kernel-abi-stablelists = %{kernel}
BuildRequires:    kernel-devel-uname-r = %{kernel}.%{_arch}

Requires:         kernel-uname-r >= %{kernel}.%{_arch}

Provides:         installonlypkg(kernel-module)
Provides:         kernel-modules >= %{kernel}.%{_arch}

Requires(posttrans): %{_sbindir}/depmod
Requires(postun):    %{_sbindir}/depmod

Requires(posttrans): %{_sbindir}/weak-modules
Requires(postun):    %{_sbindir}/weak-modules

Requires(posttrans): %{_bindir}/sort
Requires(postun):    %{_bindir}/sort


%if %{epoch}
Obsoletes:        kmod-%{pkg} < %{epoch}:
%endif
Obsoletes:        kmod-%{pkg} = %{?epoch:%{epoch}:}%{version}


%description
This package provides the Dell PERC2, 2/Si, 3/Si, 3/Di, Adaptec Advanced Raid
Products, HP NetRAID-4M, IBM ServeRAID & ICP SCSI driver (%{pkg}) driver.
Compared to the in-kernel driver this driver re-enables support for deprecated
adapters:

- 0x1011:0x0046:0x103C:0x10C2: HP NetRAID-4M
- 0x1011:0x0046:0x9005:0x0364: Adaptec 5400S (Mustang)
- 0x1011:0x0046:0x9005:0x0365: Adaptec 5400S (Mustang)
- 0x1011:0x0046:0x9005:0x1364: Dell PERC 2/QC
- 0x1028:0x0001:0x1028:0x0001: DELL PERC 2/Si (Iguana)
- 0x1028:0x0002:0x1028:0x0002: DELL PERC 3/Di (Opal)
- 0x1028:0x0002:0x1028:0x00D1: DELL PERC 3/Di (Viper)
- 0x1028:0x0002:0x1028:0x00D9: DELL PERC 3/Di (Lexus)
- 0x1028:0x0003:0x1028:0x0003: DELL PERC 3/Si (SlimFast)
- 0x1028:0x0004:0x1028:0x00D0: DELL PERC 3/Di (Iguana FlipChip)
- 0x1028:0x000A:0x1028:0x0106: DELL PERC 3/Di (Jaguar)
- 0x1028:0x000A:0x1028:0x011B: DELL PERC 3/Di (Dagger)
- 0x1028:0x000A:0x1028:0x0121: DELL PERC 3/Di (Boxster)
- 0x9005:0x0200:0x9005:0x0200: Themisto Jupiter Platform
- 0x9005:0x0283:0x9005:0x0283: Catapult
- 0x9005:0x0284:0x9005:0x0284: Tomcat
- 0x9005:0x0285: Adaptec Catch All
- 0x9005:0x0285:0x1014:0x02F2: IBM 8i (AvonPark)
- 0x9005:0x0285:0x1014:0x0312: IBM 8i (AvonPark Lite)
- 0x9005:0x0285:0x1028: Dell Catchall
- 0x9005:0x0285:0x1028:0x0287: Perc 320/DC
- 0x9005:0x0285:0x1028:0x0291: CERC SATA RAID 2 PCI SATA 6ch (DellCorsair)
- 0x9005:0x0285:0x103C:0x3227: AAR-2610SA PCI SATA 6ch
- 0x9005:0x0285:0x17AA: Legend Catchall
- 0x9005:0x0285:0x17AA:0x0286: Legend S220 (Legend Crusader)
- 0x9005:0x0285:0x17AA:0x0287: Legend S230 (Legend Vulcan)
- 0x9005:0x0285:0x9005:0x0285: Adaptec 2200S (Vulcan)
- 0x9005:0x0285:0x9005:0x0286: Adaptec 2120S (Crusader)
- 0x9005:0x0285:0x9005:0x0287: Adaptec 2200S (Vulcan-2m)
- 0x9005:0x0285:0x9005:0x0288: Adaptec 3230S (Harrier)
- 0x9005:0x0285:0x9005:0x0289: Adaptec 3240S (Tornado)
- 0x9005:0x0285:0x9005:0x028A: ASR-2020ZCR SCSI PCI-X ZCR (Skyhawk)
- 0x9005:0x0285:0x9005:0x028B: ASR-2025ZCR SCSI SO-DIMM PCI-X ZCR (Terminator)
- 0x9005:0x0285:0x9005:0x028E: ASR-2020SA SATA PCI-X ZCR (Skyhawk)
- 0x9005:0x0285:0x9005:0x028F: ASR-2025SA SATA SO-DIMM PCI-X ZCR (Terminator)
- 0x9005:0x0285:0x9005:0x0290: AAR-2410SA PCI SATA 4ch (Jaguar II)
- 0x9005:0x0285:0x9005:0x0292: AAR-2810SA PCI SATA 8ch (Corsair-8)
- 0x9005:0x0285:0x9005:0x0293: AAR-21610SA PCI SATA 16ch (Corsair-16)
- 0x9005:0x0285:0x9005:0x0294: ESD SO-DIMM PCI-X SATA ZCR (Prowler)
- 0x9005:0x0285:0x9005:0x0296: ASR-2240S (SabreExpress)
- 0x9005:0x0285:0x9005:0x0297: ASR-4005
- 0x9005:0x0285:0x9005:0x0298: ASR-4000 (BlackBird)
- 0x9005:0x0285:0x9005:0x0299: ASR-4800SAS (Marauder-X)
- 0x9005:0x0285:0x9005:0x029A: ASR-4805SAS (Marauder-E)
- 0x9005:0x0285:0x9005:0x02A4: ICP9085LI (Marauder-X)
- 0x9005:0x0285:0x9005:0x02A5: ICP5085BR (Marauder-E)
- 0x9005:0x0286: Adaptec Rocket Catch All
- 0x9005:0x0286:0x1014:0x9540: IBM 8k/8k-l4 (Aurora Lite)
- 0x9005:0x0286:0x1014:0x9580: IBM 8k/8k-l8 (Aurora)
- 0x9005:0x0286:0x9005:0x028C: ASR-2230S/ASR-2230SLP PCI-X (Lancer)
- 0x9005:0x0286:0x9005:0x028D: ASR-2130S (Lancer)
- 0x9005:0x0286:0x9005:0x029B: AAR-2820SA (Intruder)
- 0x9005:0x0286:0x9005:0x029C: AAR-2620SA (Intruder)
- 0x9005:0x0286:0x9005:0x029D: AAR-2420SA (Intruder)
- 0x9005:0x0286:0x9005:0x029E: ICP9024RO (Lancer)
- 0x9005:0x0286:0x9005:0x029F: ICP9014RO (Lancer)
- 0x9005:0x0286:0x9005:0x02A0: ICP9047MA (Lancer)
- 0x9005:0x0286:0x9005:0x02A1: ICP9087MA (Lancer)
- 0x9005:0x0286:0x9005:0x02A2: ASR-3800 (Hurricane44)
- 0x9005:0x0286:0x9005:0x02A3: ICP5445AU (Hurricane44)
- 0x9005:0x0286:0x9005:0x02A6: ICP9067MA (Intruder-6)
- 0x9005:0x0286:0x9005:0x0800: Callisto Jupiter Platform
- 0x9005:0x0287:0x9005:0x0800: Themisto Jupiter Platform
- 0x9005:0x0288: Adaptec NEMER/ARK Catch All


%prep
%autosetup -p1 -c -T


%build
pushd src
%{__make} -C /usr/src/kernels/%{kernel}.%{_arch} %{?_smp_mflags} M=$PWD modules
popd


%install
%{__install} -D -t %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/%{pkg} src/%{pkg}.ko

# Make .ko objects temporarily executable for automatic stripping
find %{buildroot}/lib/modules -type f -name \*.ko -exec chmod u+x \{\} \+

# Generate depmod.conf
%{__install} -d %{buildroot}/%{_sysconfdir}/depmod.d/
for kmod in $(find %{buildroot}/lib/modules/%{kernel}.%{_arch}/extra -type f -name \*.ko -printf "%%P\n" | sort)
do
    echo "override $(basename $kmod .ko) * weak-updates/$(dirname $kmod)" >> %{buildroot}/%{_sysconfdir}/depmod.d/%{pkg}.conf
    echo "override $(basename $kmod .ko) * extra/$(dirname $kmod)" >> %{buildroot}/%{_sysconfdir}/depmod.d/%{pkg}.conf
done


%clean
%{__rm} -rf %{buildroot}


%triggerin -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/%{pkg}/%{pkg}.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%triggerun -- %{name}
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
printf '%s\n' "/lib/modules/%{kernel}.%{_arch}/extra/drivers/scsi/%{pkg}/%{pkg}.ko" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules


%preun
mkdir -p %{_localstatedir}/lib/rpm-state/sig-kmods
rpm -ql kmod-%{pkg}-%{?epoch:%{epoch}:}%{version}-%{release}.%{_arch} | grep '/lib/modules/%{kernel}.%{_arch}/.*\.ko$' >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove


%postun
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-remove
    if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
    then
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules --no-initramfs
    else
        printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --remove-modules
    fi
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%posttrans
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules ]
then
    while read -r MODULE
    do
        if [ -f "$MODULE" ]
        then
            printf '%s\n' "$MODULE" >> %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
        fi
    done < %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules
fi
if [ -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add ]
then
    modules=( $(cat %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add | sort -u -V -t '/' -k 5 -k 4,4) )
    rm -f %{_localstatedir}/lib/rpm-state/sig-kmods/weak-modules-add
    printf '%s\n' "${modules[@]}" | %{_sbindir}/weak-modules --add-modules
fi
if [ -d %{_localstatedir}/lib/rpm-state/sig-kmods ]
then
    rmdir --ignore-fail-on-non-empty %{_localstatedir}/lib/rpm-state/sig-kmods
fi


%files
%defattr(644,root,root,755)
/lib/modules/%{kernel}.%{_arch}
%license licenses
%config(noreplace) %{_sysconfdir}/depmod.d/%{pkg}.conf


%changelog
* Fri Jul 12 2024 Peter Georg <peter.georg@physik.uni-regensburg.de> - 1:5.14.0~362.18.1-2
- Do not compress kernel modules

* Fri Feb 16 2024 Kmods SIG <sig-kmods@centosproject.org> - 1:5.14.0~362.18.1-1
- kABI tracking kmod package (kernel >= 5.14.0-362.18.1.el9_3)
